import numpy as np
import pandas as pd

df = pd.read_csv('data/ordered_products_complete.csv')
print(df.head())

df2 = df[['order_purchase_timestamp','order_item_id']]
df2.columns = ['time', 'quantity']
df2['time'] = pd.to_datetime(df2['time'])

df3 = df2['quantity'].groupby(df2['time'].dt.to_period('D')).sum()
print(df3.head())
df3.to_csv('./data/data_per_day.csv')