import pandas as pd
import numpy as np


orders = pd.read_csv("./data/olist_orders_dataset.csv", engine='python')
ordered_products = pd.read_csv("./data/olist_order_items_dataset.csv", engine='python')
product_info = pd.read_csv("./data/olist_products_dataset.csv", engine='python')

df2 = orders.merge(ordered_products, on='order_id')
#print(df2.head())
df3 = df2.merge(product_info, on='product_id')
#print(df3.head(1))
print(len(df3))

df4 = df3.loc[df3['product_category_name'] == 'beleza_saude']

print(df4.columns)
final_df = df4[['order_purchase_timestamp', 'order_item_id']]
print(final_df.head())

final_df.to_csv('./data/ordered_products_complete.csv')